from django.shortcuts import render, redirect
from django.contrib.auth.views import LoginView, LogoutView
from receipts.models import Account
from django.urls import reverse_lazy

from django.contrib.auth import authenticate, login
from accounts.form import RegisterUserForm


# Create your views here.


class AccountLoginView(LoginView):
    model = Account
    template = "registration/login.html"


class AccountLogoutView(LogoutView):
    model = Account
    success_url = reverse_lazy("home")

def register(request):

    if request.method == "POST":
        form = RegisterUserForm(request.POST)
        # submit form and get the form data
        if form.is_valid():
            username = form.cleaned_data.get("username")
            #cleaned_data is where to access the validated form data
            password = form.cleaned_data.get("password1")
            form.save()
            user = authenticate(username=username, password=password)
            #use authenticate to verify user name and passwords
            #if its not valid user = None
            #should be valid .....
            if user is not None:
                login(request, user)
                return redirect("home")
    #if not send the user to the signup
    form = RegisterUserForm()
    context = {
        "form":form
    }
    return render(request, "registration/signup.html", context)
