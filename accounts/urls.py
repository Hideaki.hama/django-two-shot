from django.urls import path
from accounts.views import AccountLoginView, AccountLogoutView, register

urlpatterns = [
  path("login/", AccountLoginView.as_view(), name="login"),
  path("logout/", AccountLogoutView.as_view(), name="logout"),
  path("signup/", register, name="signup")
]